Hardware monitor icon for the system tray. Shows CPU, memory, network and disk I/O graphs. Works on Linux, BSD and Windows.

# Dependencies

PyGtk2 and python-psutil.

On Debian/Ubuntu:

```
sudo apt-get install python-gtk2 python-psutil
```

# Screenshots

![phwmon](https://gitlab.com/uploads/project/avatar/724349/phwmon.png)
